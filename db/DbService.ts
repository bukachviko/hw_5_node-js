import fsPromise from 'fs/promises';
import path from 'path';

interface NewsPost {
    id: number,
    title: string,
    text: string;
    createDate: string;
}

interface TableData {
    id: number;
    table: NewsPost[];
    schema: Record<string, string>;
}

const NewsPostSchema: Record<string, string> = {
    id: 'Number',
    title: 'String',
    text: 'String',
    createDate: 'Date',
};

const FILEDB = 'FILEDB.json';

class FileDB {
    static async registrSchema(fileName: string, schema: Record<string, string>): Promise<void> {
        try {
            const data: TableData = {
                table: [],
                id: 1,
                schema: schema,
            };

            await fsPromise.writeFile(fileName, JSON.stringify(data, null, 2));
            console.log(`Schema registered for ${fileName}`)
        } catch (err) {
            console.error(`Error creating file ${fileName}:`, err);
        }
    }

    static async getTable(tableName: string): Promise<DbService> {

        const filePath = path.join(__dirname, tableName);
        let fileData: string | null = null;
        try {
            fileData = await fsPromise.readFile(filePath, 'utf-8');
        } catch (err) {
            await this.registrSchema(filePath, NewsPostSchema);
        }
        return new DbService(filePath, NewsPostSchema);
    }
}

class DbService {
    filePath: string;
    schema: Record<string, string>

    constructor(filePath: string, schema: Record<string, string>) {
        this.filePath = filePath;
        this.schema = schema;
    }

    private async readTable(): Promise<TableData> {
        const data = await fsPromise.readFile(this.filePath, 'utf-8');
        return JSON.parse(data)
    }

    async getAll():Promise<NewsPost[]> {
        const data = await this.readTable();
        return data.table;
    };

    async getById(id: number): Promise<NewsPost | null> {
        const data = await this.readTable();
        const item = data.table.find((item: { id: number }) => item.id === id);
        return item || null;
    };

    async create(newPostData: { title: string; text: string }): Promise<NewsPost | void> {
        try {
            const createDate = (new Date()).toString();
            const currentData = await this.readTable();
            const newPost: NewsPost = {id: currentData.id, ...newPostData, createDate};
            currentData.table.push(newPost);

            currentData.id++;

            await fsPromise.writeFile(this.filePath, JSON.stringify(currentData, null, 2));
            return newPost;

        } catch (err) {
            console.error(`Error creating object`, err);
            throw err
        }
    }

    async update(id: number, newData: { title?: string, text?: string }): Promise<NewsPost | void> {
        try {
            const data = await this.readTable();
            const index = data.table.findIndex(item => item.id === id);
            if (index === -1) return;
            const updatedItem = {...data.table[index], ...newData};
            data.table[index] = updatedItem;
            await fsPromise.writeFile(this.filePath, JSON.stringify(data, null, 2));

            return updatedItem;
        } catch (err) {
            console.error(`Error updating object`, err);
            throw err;
        }
    }

    async delete(id: number): Promise<number | void> {
        try {
            const data = await this.readTable();
            const index = data.table.findIndex((item => item.id === id))

            data.table.splice(index, 1);
            await fsPromise.writeFile(this.filePath, JSON.stringify(data, null, 2));
            return id;
        } catch (err) {
            console.log(`Error deleting object`, err);
            throw err;
        }
    }
}

(async () => {
    try {
        const dbServiceInstance = await FileDB.getTable(FILEDB);
        const tableData = await dbServiceInstance.getAll();
        // const tableData = await dbServiceInstance.create({title:'A good night', text:'hello man'});
        // const tableData = await dbServiceInstance.getById(3);
        // const tableData = await dbServiceInstance.update(1, {title: 'Hello', text:'We should to go to park'});
        // const tableData = await dbServiceInstance.delete(1);

    } catch (err) {
        console.log('Error:', err);
    }
})();

// У своєму фрагменті коду використовую паттерн Singleton, який використовується в методі
// getTable класу FileDB, який створює один екземпляр DbService для кожного файлу бази
// даних та повертає його.
// Таким чином, клас DbService для кожного файлу бази даних буде
// створений лише один раз, і при наступних викликах методу getTable для цього ж самого файлу
// буде повертатися той самий об'єкт DbService.
// Це дозволяє забезпечити централізований доступ до бази даних і уникнути створення
// зайвих екземплярів класу DbService для одного й того ж файлу бази даних.

