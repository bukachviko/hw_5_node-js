//-----------------------------------ШАБЛОНИ ПРОЕКТУВАННЯ -----------------------
//SINGLETON - обєкт який є в системі в 1 екземплярі + до нього є якась глобальна точка доступу
//Ex: корзина або карта клієнта

//1 підхід): створення глобальної змінної та потім до неї звератися, а потм увесь код обгорнути у модуль;
//2 підхід): визначити singleton всередені обєкта:
// class Counter {
//     constructor() {
//         if (typeof Counter.instance === 'object') {
//             return Counter.instance;
//         }
//         this.count = 0;
//         Counter.instance = this;
//         return this
//     }
//     getCount() {
//         return this.count;
//     }
//     increaseCount() {
//         return this.count++;
//     }
// }
// const myCount1 = new Counter();
// const myCount2 = new Counter();
//
// myCount1.increaseCount()
// myCount1.increaseCount()
// myCount2.increaseCount()
// myCount2.increaseCount()
//
// console.log(myCount2.getCount());  //4
// console.log(myCount1.getCount());  //4
// //однакові значення лічильника бо 2 обєкта ссилаються на синглтон та змінюють його 4 раза

//-----------------FACTORY METHOD- фабричний метод - створення класса, який буде допомогати створювати
// визначені обєкти на основі вхідних данних. Коли потрібно багато стврювати однотипних обєктів -обєктів з однаковою
// структурою але з різними данними. При чому ці обєкти можуть мати як властивості так і методи

// class Bmw {
//     constructor(model, price, maxSpeed) {
//         this.model = model;
//         this.price = price;
//         this.maxSpeed = maxSpeed;
//
//     }
// }
// class BmwFactory {
//     create(type) {
//         if (type === 'X5')
//             return new Bmw(type, 103000, 300);
//         if (type === 'X6')
//             return new Bmw(type, 110000, 400);
//     }
// }
//
// const factory = new BmwFactory();
//
// const x5 = factory.create('X5');
// const x6 = factory.create('X6');
//
// console.log(x5) //Bmw { model: 'X5', price: 103000, maxSpeed: 300 }
// console.log(x6) //Bmw { model: 'X6', price: 110000, maxSpeed: 400 }
//


//----------------------ABSTRACT FACTORY - абстрактная фабрика - створює інтерфейс який групує створенні раніше фабрики,
//які логічно звязані друг з другом

// function bmwProducer(kind) {
//     return kind === 'sport' ? sportCarFactory : familyCarFactory
// }
//
// //Factories
// function sportCarFactory() {
//     return new S320();
// };
//
// function familyCarFactory() {
//     return new X5();
// };
// class S320 {
//     info() {
//         return "S320 is a Sport car!"
//     }
// };
//
// class X5 {
//     info() {
//         return "X5 is a Family car !"
//     }
// }
//
// //init abstract factory of sport cars
// const produce = bmwProducer('sport');
//
// //car producing (Factory)
// const myCar = new produce();
//
// console.log(myCar.info()); //S320 is a Sport car!

//----------------STRATEGY - СТРАТЕГІЯ-- обєднує сложні алгоритми в класи, а після в залежності від умовб динамічно звертається до того чи іного класу,
// тим самим динамічно заміняє їх друго другом в процессі виконання прогрми
// function baseStrategy(amount) {
//     return amount;
// }
// function premiumStrategy(amount) {
//     return amount * 0.85;
// }
// function platinumStrategy(amount) {
//     return amount * 0.65
// }
//
// class AutoCard {
//     constructor(discount) {
//         this.discount = discount;
//         this.amount = 0;
//     }
//     checkout() {
//         return this.discount(this.amount);
//     }
//     setAmount(amount) {
//         this.amount = amount;
//     }
// }
//
// baseCustomer = new AutoCard(baseStrategy);
// premiumCustomer = new AutoCard(premiumStrategy);
// platinumCustomer = new AutoCard(platinumStrategy);
//
// baseCustomer.setAmount(50000);
// console.log(baseCustomer.checkout());
//
// premiumCustomer.setAmount(50000);
// console.log(premiumCustomer.checkout());
//
// platinumCustomer.setAmount(50000);
// console.log(platinumCustomer.checkout());


// -------------------OBSERVER -Наблюдач --повіденіковий пат проектування який створює механізм підписки та дозволяющий одним обєктам
//слідкувати за змінами інших обєктах(тіпа store у реакті)

//EX:підписка на новосні ленти ресурса(по реєстрації емейла)
// class AutoNews {
//     constructor() {
//         this.news = '';
//         this.actions = [];
//     }
//     setNews(text) {
//         this.news = text;
//         this.notifyAll();
//     }
//     notifyAll() {
//         return this.actions.forEach(subs => subs.inform(this));
//     }
//     register(observer) {
//         this.actions.push(observer);
//     }
//     unregister(observer) {
//         this.actions = this.actions.filter(el => !(el instanceof observer));
//     }
// }
// class Jack {
//     inform(message) {
//         console.log(`Jack has been informed about: ${message.news}`)
//     }
// }
// class Max {
//     inform(message) {
//         console.log(`Max has been informed about: ${message.news}`)
//     }
// }
//
// const autoNews = new AutoNews();
//
// autoNews.register(new Jack());
// autoNews.register(new Max());
//
// autoNews.setNews('New MG 3 cross cost is 30000')

//----------------------STATE

// class OrderStatus {
//     constructor(name, nextStatus) {
//         this.name = name;
//         this.nextStatus = nextStatus;
//     }
//     next() {
//         return new this.nextStatus();
//     }
// }
//
// class WaitingForPayment extends OrderStatus {
//     constructor() {
//         super('waitingForPayment', Shipping);
//     }
// }
// class Shipping extends OrderStatus {
//     constructor() {
//         super('shipping', Delivered);
//     }
// }
// class Delivered extends OrderStatus {
//     constructor() {
//         super('delivered', Delivered);
//     }
// }
//
// class Order {
//     constructor() {
//         this.state = new WaitingForPayment();
//     }
//     nextState() {
//         this.state = this.state.next();
//     };
//     cancelOrder() {
//         this.state.name === 'waitingForPayment' ?
//             console.log('Order is cancelled!') :
//             console.log('Order can not be canceled!');
//     }
// }
//
// const myOrder = new Order();
// console.log(myOrder.state.name)  //waitingForPayment
// myOrder.cancelOrder();
//
// myOrder.nextState();
// console.log(myOrder.state.name) //shipping
//
// myOrder.nextState();
// console.log(myOrder.state.name) //delivered
// myOrder.cancelOrder();

